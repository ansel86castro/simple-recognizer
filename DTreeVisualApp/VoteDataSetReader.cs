﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.CommonTypes;

namespace DTreeVisualApp
{
    public class VoteDataSetReader:DataSetReader
    {
        protected override float[] EncodeClassOutput(float k)
        {
            float value = k == 0 ? 0.1f : 0.9f;
            return new float[] { value };
        }
        public float Encode(string value)
        {
            return EncodeValue(value);
        }
        public string DecodeC(float value)
        {
            return DecodeClass(value);
        }
        protected override float EncodeValue(string value)
        {
            if (value == "'y'" || value == "y")
                return 0.9f;
            if (value == "'n'" || value == "n")
                return 0.1f;
            return 0.5f;
        }

        protected override float EncodeClass(string value)
        {
            if (value == "'republican'")
                return 0;
            if (value == "'democrat'")
                return 1;

            throw new ArgumentException("valor no valido");
        }

        protected override string DecodeValue(float value)
        {
            if (value == 0.1f)
                return "'n'";
            else if (value == 0.9f)
                return "'y'";
            return "'?'";
        }

        protected override string DecodeClass(float clas)
        {
            if (clas == 0)
                return "'republican'";
            if (clas == 1)
                return "'democrat'";

            throw new ArgumentException("valor no valido");
        }
    }
}
