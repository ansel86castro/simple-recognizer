﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.DesitionTree;

namespace DTreeVisualApp
{
    public partial class TreeViewForm : Form
    {
        IDTree tree;
        List<string> attrNames;
        Bitmap bitmap;
        Size size;
        public TreeViewForm(IDTree tree)
        {
            InitializeComponent();

            this.tree = tree;

            attrNames = new List<string>() 
            { 
                "handicapped-infants",
                "water-project-cost-sharing",
                "adoption-of-the-budget-resolution",
                "physician-fee-freeze",
                "el-salvador-aid",
                "religious-groups-in-schools",
                "anti-satellite-test-ban",
                "aid-to-nicaraguan-contras",
                "mx-missile",
                "immigration",
                "synfuels-corporation-cutback",
                "education-spending",
                "superfund-right-to-sue",
                "crime",
                "duty-free-exports",
                "export-administration-act-south-africa"                
            };
            size = tree.GetTreeSize(encodeClass, encodeValue, Font, attrNames);
            bitmap = new Bitmap(size.Width, size.Height);

            Draw();

            pictureBox1.BackgroundImage = bitmap;
            pictureBox1.Width = bitmap.Width;
            pictureBox1.Height = bitmap.Height;            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
        
        }

        void Draw()
        {
            Rectangle r = new Rectangle(0, 0, size.Width, size.Height);

            Graphics g = Graphics.FromImage(bitmap);

            g.Clear(Color.White);

            tree.Draw(g, r, attrNames, Font, encodeClass, encodeValue);
        }

        string encodeClass(float c)
        {
            if (c == 0.1f) return "republican";
            else if (c == 0.9f) return "democrat";
            else
                throw new ArgumentException();
        }
        string encodeValue(float v)
        {
            if (v == 0.1f) return "NO";
            if (v == 0.5f) return "?";
            if (v == 0.9f) return "SI";

            return " ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bitmap == null) return;

            saveFileDialog1.FileName = null;
            saveFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                bitmap.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Gif);
            }
        }
    }
}
