﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.CommonTypes;
using IA.DesitionTree;

namespace DTreeVisualApp
{
    public partial class Form1 : Form
    {
        VoteDataSetReader dset,temp;
        TrainingSet tset;
        IDTree dTree;
        Console console;
        public Form1()
        {
            InitializeComponent();
            console = new Console();
            temp = new VoteDataSetReader();
        }

        private void cToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(dTree==null)
            {
                MessageBox.Show("You should load a desition tree before perform this action");
                return;
            }
            float[] input = GetInput(tbInput.Text);
            float v = dTree.CalculateOutputs(input);
            if (v == 0.1f)
                lbResult.Text = "republican";
            else
                lbResult.Text = "democrat";
        }

        private float[] GetInput(string line)
        {
            int start = 0;
            List<float> input = new List<float>();
            
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ',')
                {
                    input.Add(temp.Encode(line.Substring(start, i - start)));
                    start = i + 1;
                }
            }
            input.Add(temp.Encode(line.Substring(start, line.Length - start)));

            return input.ToArray<float>();
        }

        private void trainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!LoadTrainingSet())
                return;
            if (console.IsDisposed)
                console = new Console();

            float err=DTree.Train(tset, s => console.WriteLine(s), out dTree);
            
            float presition = (1 - err) * 100;

            MessageBox.Show("Presition Achieved of " + presition+"%");
        }

        bool LoadDataSet()
        {
            openFileDialog1.FileName = null;
            openFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                dset = new VoteDataSetReader();
                dset.Read(openFileDialog1.FileName);
                return true;
            }
            return false;
        }
        bool LoadTrainingSet()
        {
            if (dset == null && !LoadDataSet())
                return false;
            GroupsDialog gd = new GroupsDialog();
            gd.ShowDialog(this);
            if (gd.DialogResult == DialogResult.OK)
            {
                tset = dset.GetTraningSet(gd.GroupCount);
                return true;
            }
            return false;

        }

        private void buildToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadTrainingSet();
            dTree = DTree.Build(tset.Group(0));
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!showToolStripMenuItem.Checked)
            {
                console.Show();
                showToolStripMenuItem.Checked = true;
            }
            else
            {
                console.Hide();
                showToolStripMenuItem.Checked = false;
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
             cToolStripMenuItem_Click(null, null);
        }

        private void openDesitionTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string file=OpenFile();
            if(file!=null)
            {
                dTree = Serializer.Load<IDTree>(file);
            }
        }

        string OpenFile()
        {
            openFileDialog1.FileName = null;
            openFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
                return openFileDialog1.FileName;
            return null;
        }
        string SaveFile()
        {
            saveFileDialog1.FileName = null;
            saveFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
                return saveFileDialog1.FileName;
            return null;
        }

        private void saveDesitionTreeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string file = SaveFile();
            if (file != null)
            {
                Serializer.Save(dTree, file);
            }
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dTree != null)
            {
                TreeViewForm view = new TreeViewForm(dTree);
                view.Show();
            }
            else
            {
                MessageBox.Show("You should load a desition tree before perform this action");
            }
        }
        
    }
}
