﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DTreeVisualApp
{
    public partial class GroupsDialog : Form
    {
        public GroupsDialog()
        {
            InitializeComponent();

            group.Value = 1;
        }

        public int GroupCount
        {
            get { return (int)group.Value; }
        }       

        private void Ok_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
