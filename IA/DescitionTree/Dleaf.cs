﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace IA.DesitionTree
{
    [Serializable]
    public class DTreeLeaf: IDTree
    {
        float attrObjValue;

        public DTreeLeaf(float value)
        {
            attrObjValue = value;
        }

        #region IDTree Members

        public bool IsLeaf
        {
            get { return true; }
        }

        public List<TreeLink> Nodes
        {
            get
            {
                return null;
            }
        }

        public int AttrIndex
        {
            get
            {
                return 0;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public float ObjValue
        {
            get
            {
                return attrObjValue;
            }
            set
            {
                attrObjValue = value;
            }
        }

        public float CalculateOutputs(float[] input)
        {
            return attrObjValue;
        }

        /// <summary>
        /// El tamaño de una hoja es el tamaño del texto mas un pixel de padding a cada extremo
        /// </summary>
        /// <param name="encoder">retorna el string del valor correspondiente del atributo objectivo</param>
        /// <param name="fond">Font del string</param>
        /// <returns>tamaño horizontal y vertical en p.x y p.y respectivamente de pixeles necesarios para dibujar el arbol</returns>
        public Size GetTreeSize(EncoderClassFunction fc, EncoderValueFunction fv, Font font, List<string> attrNames)
        {
            //El tamaño de una hoja es el tamaño del texto mas un pixel de padding a cada extremo
            string attr = fc(attrObjValue);
            int strSize = (attr.Length * (int)font.SizeInPoints);
            return new Size(strSize + DTree.Padding, font.Height);
        }

        public void Draw(Graphics g, Rectangle bounds, List<string> attrNames, Font font, EncoderClassFunction fc, EncoderValueFunction fv)
        {
            //dibuja el valor del atributo Objecitivo
            string s=fc(attrObjValue);
            g.DrawString(s, font, Brushes.Green, bounds.X, bounds.Y);
            //g.DrawString(" [" + fv(branchValue) + "]", font, Brushes.Red, bounds.X + s.Length * (int)font.SizeInPoints, bounds.Y);

            g.DrawEllipse(Pens.Black, bounds);
        }
       
        #endregion
      
    }
}
