﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.CommonTypes;
using System.Drawing;
namespace IA.DesitionTree
{    
    public interface IDTree 
    {        
        bool IsLeaf { get; }

        List<TreeLink> Nodes { get; }

        int AttrIndex { get; set; }

        float ObjValue { get; set; }

        float CalculateOutputs(float[] input);        

        Size GetTreeSize(EncoderClassFunction fc, EncoderValueFunction fv, Font font, List<string> attrNames);        

        void Draw(Graphics g, Rectangle bounds, List<string> attrNames, Font font, EncoderClassFunction fc, EncoderValueFunction fv);
        
    }

    public static class IEnumerableExtension
    {
        public static bool IsEmpty<T>(this IEnumerable<T> source)
        {
            bool empty = true;
            foreach (var e in source)
            {
                if (!empty)
                    return false;
                empty = false;
            }
            return empty;
        }
    }

    public delegate string EncoderValueFunction(float v);

    public delegate string EncoderClassFunction(float c);
}
