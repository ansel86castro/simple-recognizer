﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using IA.CommonTypes;
using IA.DesitionTree;

namespace IA.DesitionTree
{
    [Serializable]
    public class DTree : IDTree
    {
        int attrIndex;
        List<TreeLink> nodes;

        public static int LineHeight=20;
        public static int Padding = 2;

        /// <summary>
        /// Crea un Arbol de Decision 
        /// </summary>
        /// <param name="name">Nombre del Atributo que se va poner como etiqueta del arbol</param>
        /// <param name="nodes">Lista de hijos de este arbol</param>
        public DTree(int attrIndex, List<TreeLink> nodes)
        {
            this.attrIndex = attrIndex;
            this.nodes = nodes;
        }
        /// <summary>
        /// Crea un Arbol de Desicion Vacio
        /// </summary>
        public DTree() : this(0, new List<TreeLink>()) { }

        #region IDTree<T> Members

        public bool IsLeaf
        {
            get { return false; }
        }

        public List<TreeLink> Nodes
        {
            get { return nodes; }
        }

        public int AttrIndex
        {
            get
            {
                return attrIndex;
            }
            set
            {
                attrIndex = value;
            }
        }

        public float ObjValue
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public float CalculateOutputs(float[] input)
        {
            float v = input[attrIndex];
            foreach (var n in nodes)
            {
                if (v <= n.value)
                    return n.node.CalculateOutputs(input);
            }
            return nodes[nodes.Count - 1].node.CalculateOutputs(input);
        }

        #endregion
        public static float Train(TrainingSet set, MessageHandler notify)
        {
            IDTree tree;
            return Train(set, notify, out tree);
        }
        public static float Train(TrainingSet set, MessageHandler notify, out IDTree tree)
        {
            tree = null;
            if (set.GroupsCount == 0) return 0;

            float totalErr = 0, err = 0, output;

            List<int> attrs = new List<int>(set[0].Input.Length);
            for (int i = 0; i < attrs.Capacity; i++)
                attrs.Add(i);

            if (set.GroupsCount == 1)
            {
                if (notify != null)
                    notify("Training the Tree with all examples");

                tree = Build(set.Group(0), attrs);

                if (notify != null)
                    notify("Validating the network with all examples");

                foreach (var e in set.Group(0))
                {
                    output = tree.CalculateOutputs(e.Input);
                    float t = e.Output[0] - output;
                    err += t * t;
                }
                totalErr += err / set.GroupCount(0);
            }
            else
            {
                for (int i = 0; i < set.GroupsCount; i++)
                {
                    //entrena la red con n-1 grupos de elementos
                    if (notify != null)
                        notify("Training the n -{" + i + "} groups");

                    tree = Build(set.KFold_Training(i), attrs);

                    //prueba la red con los grupos restantes y calcula el error como la media de los errores parciales
                    if (notify != null)
                        notify("Validating with group " + i);
                    foreach (var e in set.Group(i))
                    {
                        output = tree.CalculateOutputs(e.Input);
                        float t = e.Output[0] - output;
                        t = t * t;
                        err += t;
                        if (notify != null)
                            notify("    Error cuadratic in prediction " + t + " element output=" + e.Output[0].ToString() + " system output=" + output.ToString());
                    }
                    totalErr += err / set.GroupCount(i);
                }
            }

            return totalErr / set.GroupsCount;
        }

        public static IDTree Build(IEnumerable<Element> elements)
        {
            List<int> attrs = new List<int>(elements.First().Input.Length);
            for (int i = 0; i < attrs.Capacity; i++)
                attrs.Add(i);
            return Build(elements, attrs, elements);
        }

        public static IDTree Build(IEnumerable<Element> elements, List<int> attrs)
        {
            return Build(elements, attrs, elements);
        }

        private static IDTree Build(IEnumerable<Element> elements, List<int> attrs, IEnumerable<Element> source)
        {
            float objValue;
            IDTree root;

            //si todos los elementos tienen el mismo valor para el atributo objetivo
            if (HasSameValue(elements, out objValue))
            {
                //poner el valor de objValue como etiqueta del arbol
                root = new DTreeLeaf(objValue);

            }
            else if (attrs.Count == 0)
            {
                //poner valor mas comun de attrObj en elements como etiqueta de root
                objValue = GetMostCommonValue(elements);
                root = new DTreeLeaf(objValue);
            }
            else
            {
                root = new DTree();


                Dictionary<float, float> attrValues;

                //tomar mejor attributo que clasifica a elements
                root.AttrIndex = GetBestClasifyValue(elements, attrs, out attrValues);

                //Atributes =Atributes - {A}          
                attrs.Remove(root.AttrIndex);

                attrValues = CalculateValuesCount(source, root.AttrIndex);

                //por cada cada posible valor del mejor atributo
                foreach (var v in attrValues.Keys)
                {
                    var subSet = from e in elements
                                 where e.Input[root.AttrIndex] == v
                                 select e;

                    if (subSet.IsEmpty())
                        root.Nodes.Add(new TreeLink(v, new DTreeLeaf(GetMostCommonValue(elements))));

                    else
                        root.Nodes.Add(new TreeLink(v, Build(subSet, attrs, source)));

                }
                attrs.Add(root.AttrIndex);

            }
            if (root.Nodes != null)
                root.Nodes.Sort((x, y) => x.value.CompareTo(y.value));
            return root;
        }
        /// <summary>
        /// Calcula el atributo que tenga mayor ganancia de informacion 
        /// </summary>
        /// <param name="elements">elementos</param>
        /// <param name="attrs">lista de atributos</param>
        /// <returns>Atributo con mayor ganancia</returns>
        private static int GetBestClasifyValue(IEnumerable<Element> elements, List<int> attrs, out Dictionary<float, float> values)
        {
            float bestGain = 0;
            int bestAttr = 0;
            Dictionary<float, float> temp;
            values = null;

            foreach (var attr in attrs)
            {
                float gain = Gain(elements, attr, out temp);
                if (bestGain < gain)
                {
                    bestGain = gain;
                    bestAttr = attr;
                    values = temp;
                }
            }

            return bestAttr;
        }
        /// <summary>
        /// Calcula cual es el valor mas comun del atributo objetivo
        /// </summary>
        /// <param name="elements">Elementos</param>
        /// <returns>Valor con mayor probabilidad</returns>
        private static float GetMostCommonValue(IEnumerable<Element> elements)
        {
            var probs = CalculateProbabilitys(elements);

            float max = 0;
            int best = 0;

            for (int i = 0; i < probs.Length; i++)
            {
                if (probs[i].Value > max)
                {
                    max = probs[i].Value;
                    best = i;
                }
            }

            return probs[best].Key;
        }
        /// <summary>
        /// Dice si los Elementos tienen el mismo valor para el atributo objetivo
        /// </summary>
        /// <param name="elements">Elementos</param>
        /// <param name="objValue">valor del atributo objetivo</param>
        /// <returns></returns>
        private static bool HasSameValue(IEnumerable<Element> elements, out float objValue)
        {
            objValue = elements.First().Output[0];

            foreach (var e in elements)
            {
                if (e.Output[0] != objValue)
                    return false;
            }
            return true;
        }
       
        public void Draw(Graphics g, Rectangle bounds, List<string> attrNames, Font font, EncoderClassFunction fc, EncoderValueFunction fv)
        {
            int middle = bounds.Left + bounds.Width / 2;

            Rectangle headRect = GetHeaderRect(font, attrNames[attrIndex], bounds);

            //dibujar el nombre del attributo de la raiz del arbol
            g.DrawString(attrNames[AttrIndex], font, Brushes.Black, headRect.X, headRect.Y);

            //dibujar el valor del atributo del la rama del arbol
                    //g.DrawString(" [" + fv(branchValue) + "]", font, Brushes.Red, headRect.X + (int)(attrNames[attrIndex].Length * (int)font.SizeInPoints), headRect.Y);
            g.DrawRectangle(Pens.Black, headRect);

            int rx = bounds.X;
            int ry = bounds.Y + font.Height + LineHeight;

            for (int i = 0; i < nodes.Count; i++)
            {
                Size size = nodes[i].node.GetTreeSize(fc, fv, font,attrNames);

                //Calcular el rectangulo donde voy a dibujar el subarbol
                Rectangle r = new Rectangle(rx, ry, size.Width, size.Height);
                              
                //dibujar la rama al subarbol
                g.DrawLine(Pens.Blue, middle, headRect.Bottom, r.X + r.Width / 2, r.Y);

                //dibujar el valor de la rama
                Rectangle hRec;
                if (nodes[i].node.IsLeaf)                
                    hRec = DTree.GetHeaderRect(font, fc(nodes[i].node.ObjValue), r);                
                else
                    hRec = DTree.GetHeaderRect(font, attrNames[nodes[i].node.AttrIndex], r);
                g.DrawString(fv(nodes[i].value), font, Brushes.Red, hRec.X, hRec.Y - font.Height);

                //dibujar el subarbol
                nodes[i].node.Draw(g, r, attrNames, font, fc, fv);

                rx += size.Width;

            }

        }

        public static Rectangle GetHeaderRect(Font font, string rootTag , Rectangle bound)
        {
            Size size = new Size(rootTag.Length * (int)font.SizeInPoints, font.Height);
            
            Point location = new Point(bound.X + (bound.Width - size.Width) / 2, bound.Y);

            return new Rectangle(location, size);

        }       

        /// <summary>
        /// El tamaño del arbol horizontal es la suma del tamaño de cada nodo y el vertical es el maximo 
        /// tamaño vertical de sus nodos
        /// </summary>
        /// <param name="encoder"></param>
        /// <param name="fond"></param>
        /// <returns>p.X -tamaño horizontal p.Y -tamaño vertical </returns>
        public Size GetTreeSize(EncoderClassFunction fc, EncoderValueFunction fv, Font font, List<string> attrNames)
        {
            Size size = new Size(), temp;
            foreach (var n in nodes)
            {
                temp = n.node.GetTreeSize(fc, fv, font,attrNames);

                int valueSize = fv(n.value).Length * (int)font.SizeInPoints;

                size.Width += Math.Max(valueSize, temp.Width);

                if (temp.Height > size.Height)
                    size.Height = temp.Height;
            }
            string s = attrNames[attrIndex];
            size.Width = Math.Max(s.Length * (int)font.SizeInPoints, size.Width);

            size.Height += font.Height + LineHeight + Padding; //alto del texto + 2 pixels de padding + 10 alto de las lineas que unen los hijos con la raiz

            return size;

        }

        #region ID3
        /// <summary>
        /// Calcula la Ganancia de Informacion de  los elementos tomando el atributo en attrIndex
        /// </summary>
        /// <param name="elements">Elementos</param>
        /// <param name="attrIndex">indice del Atributo</param>
        /// <returns></returns>
        private static float Gain(IEnumerable<Element> elements, int attrIndex, out Dictionary<float, float> values)
        {
            float gain;     //ganancia de informacion
            int total;      //cantidad total de elementos           

            var probs = CalculateProbabilitys(elements, out total);     //calcular las probabilidades de los valores del atributo objetivo 

            gain = Entropy(elements, probs);                    //calcular la entropia de los elementos 

            //calcular la cardinalidad del conjunto Ea=v)
            values = CalculateValuesCount(elements, attrIndex);

            //Gain(E,a) = H(E) - SUM( Ea=v/total * H(Ea=v) ) para todo valor v de a
            foreach (var v in values.Keys)
            {
                gain -= values[v] / total * Entropy(elements.Where(e => e.Input[attrIndex] == v));
            }

            return gain;

        }

        private static float Entropy(IEnumerable<Element> elements, KeyValuePair<float, float>[] probs)
        {
            float entropy = 0;

            //H(E)= -p1*log(p1) -p2*log(p2) - ..... - pn*log(pn)
            for (int i = 0; i < probs.Length; i++)
            {
                entropy -= probs[i].Value * (float)Math.Log(probs[i].Value, 2);
            }

            return entropy;
        }

        private static float Entropy(IEnumerable<Element> elements)
        {
            int count;
            return Entropy(elements, CalculateProbabilitys(elements, out count));
        }

        private static KeyValuePair<float, float>[] CalculateProbabilitys(IEnumerable<Element> elements)
        {
            int total;
            return CalculateProbabilitys(elements, out total);
        }

        private static KeyValuePair<float, float>[] CalculateProbabilitys(IEnumerable<Element> elements, out int total)
        {
            //diccionario que almacena valor - cantidad de ocurrencia en elemet
            Dictionary<float, float> values = new Dictionary<float, float>();

            total = 0;

            foreach (var e in elements)
            {
                total++;
                var v = e.Output[0];
                if (values.ContainsKey(v))
                    values[v]++;
                else
                    values.Add(v, 1);
            }

            //probabilidad de los valores 
            KeyValuePair<float, float>[] probs = new KeyValuePair<float, float>[values.Keys.Count];

            int i = 0;
            foreach (var v in values.Keys)
            {
                probs[i] = new KeyValuePair<float, float>(v, values[v] / total);
                i++;
            }

            return probs;
        }

        private static Dictionary<float, float> CalculateValuesCount(IEnumerable<Element> elements, int attrIndex)
        {
            Dictionary<float, float> values = new Dictionary<float, float>();

            foreach (var e in elements)
            {
                var v = e.Input[attrIndex];
                if (values.ContainsKey(v))
                    values[v]++;
                else
                    values.Add(v, 1);
            }
            return values;
        }

        #endregion

    }

}
