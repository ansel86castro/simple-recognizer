﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.DesitionTree
{
    [Serializable]
    public struct TreeLink
    {
         public float value;
         public IDTree node;
        public TreeLink(float value, IDTree Node)
        {
            this.value = value;
            this.node = Node;
        }
        public float Value { get { return value; } set { this.value=value;} }
        public IDTree Node { get { return node; } set { node = value; } }
    }
}
