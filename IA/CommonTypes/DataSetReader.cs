﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IA.CommonTypes
{
    public abstract class DataSetReader
    {        
        protected Dictionary<float,List<float[]>> data;

        protected List<float> element;

        protected int count;

        public DataSetReader()
        {
            data = new Dictionary<float,List<float[]>>();
            element = new List<float>();
        }

        public float[] GetData(float clase , int index)
        {
            return data[clase][index];
        }
        
        public List<float[]> GetData(float clase)
        {
            return data[clase];
        }

        public int Count { get { return count; } }

        public Element GetElement(float clase, int index)
        {
            return new Element(data[clase][index], EncodeClassOutput(clase));
        }
        
        public TrainingSet GetTraningSet(int groupCount)
        {            
            int elementsPerGroup = count / groupCount;
            int elementsPerClass = elementsPerGroup / data.Keys.Count;

            Dictionary<float, float[]> outputs = EncodeClassOutputs();
            Dictionary<float, Marca> marcados = new Dictionary<float, Marca>();
            List<Element>[] groups = new List<Element>[groupCount];

            InitMarcados(marcados);

            for (int k = 0; k < groupCount; k++)
            {
                groups[k] = new List<Element>(elementsPerGroup);

                //escojer el max de elementos de cada clase de forma que este al menos un elemento de cada clase en el grupo k
                FillGroup(groups[k], marcados, elementsPerClass, outputs);

                //rellenar el grupo con elementos de las primeras clases que no esten marcados
                while (groups[k].Count < elementsPerGroup)
                {
                    FillGroup(groups[k], marcados, 1, outputs);
                }
            }
            return new TrainingSet(groups, elementsPerGroup);

           
        }

        private void InitMarcados(Dictionary<float, Marca> marcados)
        {            
            foreach (var clas in data.Keys)
            {
                marcados.Add(clas, new Marca(data[clas].Count));
            }            
        }
        
        private void FillGroup(List<Element> group, Dictionary<float, Marca> marcados, int takenFromClass, Dictionary<float, float[]> outputs)
        {
            foreach (var clas in data.Keys)
            {
                //marcados[clas].Key = cantidad de elementos marcados

                if (group.Count == group.Capacity) return;                

                int taken = 0;
                if (marcados[clas].Count < data[clas].Count)//si hay elementos no marcados
                {
                    for (int i = 0; i < marcados[clas].Values.Length && taken < takenFromClass; i++)
                    {
                        if (!marcados[clas][i])
                        {
                            group.Add(new Element(data[clas][i], outputs[clas]));
                            marcados[clas][i] = true;
                            taken++;
                        }
                    }
                }
                
            }
        }          

        public virtual void Read(String filename)
        {
            Clear();
            StreamReader reader = new StreamReader(filename);
            string line = reader.ReadLine();
            while (line != null)
            {
                if (line.StartsWith("@data"))
                {
                    ReadData(reader);
                }
                line = reader.ReadLine();
            }
        }

        protected virtual void ReadData(StreamReader reader)
        {
            string line = reader.ReadLine();
            while (line != null)
            {
                if (line != "%")                
                    FormatElemet(line);
                line = reader.ReadLine();
            }
        }

        protected virtual void FormatElemet(string line)
        {
            element.Clear();
            int start=0;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == ',')
                {
                    element.Add(EncodeValue(line.Substring(start, i - start)));
                    start = i + 1;
                }
            }
            element.Add(EncodeClass(line.Substring(start, line.Length - start)));

            float[] values = new float[element.Count - 1];

            for (int i = 0; i < element.Count - 1; i++)
                values[i] = element[i];
            
            if (data.ContainsKey(element[element.Count - 1]))
                data[element[element.Count - 1]].Add(values);
            else
                data.Add(element[element.Count - 1], new List<float[]>() { values });

            count++;
        }       

        public virtual void AddData(float clase,float[]input)
        {            
            if (data.ContainsKey(clase))
                data[clase].Add(input);
            else
                data.Add(clase, new List<float[]>() { input });

            count++;
        }

        
        public virtual void Save(string filename ,bool append)
        {
            using (StreamWriter writer = new StreamWriter(filename,append))
            {
                if(!append)
                    writer.WriteLine("@data");
                foreach (var clas in data.Keys)
                {
                    foreach (var input in data[clas])
                    {

                        writer.WriteLine(DecodeInput(input) + "," + DecodeClass(clas));
                    }
                }
                writer.Close(); 
            }
        }
        
        public virtual void Save(string filename)
        {
            Save(filename, false);
        }
       
        private string DecodeInput(float[] input)
        {
            StringBuilder sb = new StringBuilder(DecodeValue(input[0]));
            for (int i = 1; i < input.Length; i++)
            {
                sb.Append("," +  DecodeValue(input[i]));
            }
            return sb.ToString();
        }

        protected Dictionary<float, float[]> EncodeClassOutputs()
        {
            Dictionary<float, float[]> outputs = new Dictionary<float, float[]>();
            foreach (var clas in data.Keys)
            {
                outputs.Add(clas, EncodeClassOutput(clas));
            }

            return outputs;
        }

        public void Clear()
        {
            count = 0;
            element.Clear();
            data.Clear();
        }

        protected abstract float[] EncodeClassOutput(float k);
        protected abstract float EncodeValue(string value);
        protected abstract float EncodeClass(string value);
        protected abstract string DecodeValue(float value);
        protected abstract string DecodeClass(float clas);
    }
    public class DataContainer
    {
        public float[] Data;
        public float AttrObj;
        public DataContainer(float[] data, float AttrObj)
        {
            this.Data = data;
            this.AttrObj = AttrObj;
        }
    }
    public class Marca
    {
        public int Count;
        public bool[] Values;

        public Marca(int count)
        {
            Values=new bool[count];
            Count = 0;
        }
        public bool this[int index]
        {
            get { return Values[index]; }
            set 
            { 
                Values[index] = value;
                if (value) 
                    Count++;
            }
        }

    }
}
