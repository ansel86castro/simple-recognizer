﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.CommonTypes
{
    public delegate void MessageHandler(string msg);

    public class TrainingSet
    {     
        int groupSize;
        List<Element>[] groups;
        public TrainingSet(List<Element>[] groups, int groupSize)
        {
            this.groupSize = groupSize;
            this.groups = groups;
        }                               
       
        public int GroupsSize { get { return groupSize; } set { groupSize = value; } }

        public int GroupsCount { get { return groups.Length; } }

        public int GroupCount(int index)
        {
            return groups[index].Count;
        }

        public Element this[int index]
        {
            get
            {
                int group = index / groups.Length;
                int i = index % groups.Length;
                return groups[group][i];
            } 
            set
            {
                int group = index / groups.Length;
                int i = index % groups.Length;
                groups[group][i]=value;
            }
        }
        public Element this[int group, int index]
        {
            get
            {
                return groups[group][index];
            }
            set
            {
                groups[group][index] = value;
            }
        }

        public IEnumerable<Element> Group(int k)
        {
            foreach (var e in groups[k])
                yield return e;
        }
       
        public IEnumerable<Element> KFold_Training(int ExcluidGroup)
        {
            for (int i = 0; i < GroupsCount; i++)
            {
                if (i != ExcluidGroup)
                {
                    foreach (var e in Group(i))
                        yield return e;
                }
            }
        }

        //public void Train(Network network)
        //{
        //    float[]output=new float[groups[0][0].Output.Length];

        //    for (int i = 0; i < GroupsCount; i++)
        //    {
        //        float err=0;

        //        //entrena la red con n-1 grupos de elementos
        //        network.Train(KFold_Training(i));

        //        //prueba la red con los grupos restantes
        //        foreach (var e in Group(i))
        //        {
        //            network.CalculateOutput(e.Input, output);
        //            err += CalculateError(e, output);
        //        }
        //    }
        //}

        //private float CalculateError(Element e, float[] output)
        //{
        //    throw new NotImplementedException();
        //}

    }
}
