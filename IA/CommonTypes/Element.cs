﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.CommonTypes
{
    public class Element
    {
        float[] input;
        float[] output;

        public Element(float[] input, float[] output)
        {
            this.input = input;
            this.output = output;
        }
        public float[] Input
        {
            get
            {
                return input;
            }
            set
            {
                input = value;
            }
        }

        public float[] Output
        {
            get
            {
                return output;
            }
            set
            {
                output = value;
            }
        }

        public override string ToString()
        {
            if (input.Length == 0 && output.Length==0) return "empty";

            StringBuilder str=new StringBuilder("Input:(" + input[0]);

            for (int i = 1; i < input.Length; i++)
                str.Append(", " + input[i]);

            str.Append(") Output:(" + output[0]);

            for (int i = 1; i < output.Length; i++)
                str.Append(", " + output[i]);

            str.Append(')');

            return str.ToString();
        }        
    }    
}
