﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace IA.NeuralNetwork
{
    public static class Miscelanea
    {
        public static Color GetColor(float p)
        {
            int k = (int)(255 * (1 - p));
            return Color.FromArgb(k, k, k);
        }
        
        public static int GetDigit(float[] output)
        {
            float max = 0;
            int best = 0;
            for (int i = 0; i < output.Length; i++)
            {
                if (output[i] > max)
                {
                    max = output[i];
                    best = i;
                }
            }
            return best;
        }
        
        public static float[] CompressData(float[,]input ,float clamp)
        {            

            float[] convInput = new float[64];

          //  var invert = Invert(input);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    convInput[i * 8 + j] = CountOnPixels(input, i * 4, j * 4) / clamp;
                }
            }
            return convInput;
        }
        public static float[,] Transpose(float[,] input)
        {
            float[,] invert = new float[input.GetLength(1), input.GetLength(0)];

            for (int i = 0; i < input.GetLength(1); i++)
                for (int j = 0; j < input.GetLength(0); j++)
                    invert[i, j] = input[j, i];

            return invert;
        }
        
        public static void ExpandData(float[] convInput, float[,] input)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    ExpandOnPixels(input, i * 4, j * 4, convInput[i * 8 + j]);
                }
            }
        }
        public static float[,] ExpandData(float[] convInput,int width,int height)
        {
            float[,] data = new float[width, height];
            ExpandData(convInput, data);
            return data;
        }

        public static void ExpandOnPixels(float[,] input, int pi, int pj, float value)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    input[pi + i, pj + j] = value;
                }
            }
        }
        
        public static float CountOnPixels(float[,]input ,int pi, int pj)
        {
            float sum = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    sum += input[pi + i, pj + j];
                }
            }
            return sum;
        }
        
        public static string InputToString(this Array input)
        {            
            string s = input.GetValue(0).ToString();
            for (int i = 1; i < input.Length; i++)
            {
                s += " ," + input.GetValue(i).ToString();
            }
            return s;
        }
        public static string InputToString<T>(this List<T> input)
        {
            string s = input[0].ToString();
            for (int i = 1; i < input.Count; i++)
            {
                s += " ," + input[i].ToString();
            }
            return s;
        }
    }

    public struct Vector2D
    {
        public int x, y;
        public static readonly Vector2D Zero;
        public static readonly Vector2D Right;
        public static readonly Vector2D Down;
        public static readonly Vector2D DiagDown;
        public static readonly Vector2D DiagUp;
        public static readonly Vector2D[] Directions;

        static Vector2D()
        {
            Zero = new Vector2D(0, 0);
            Right = new Vector2D(0, 1);
            Down = new Vector2D(1, 0);
            DiagDown = new Vector2D(1, 1);
            DiagUp = new Vector2D(-1, 1);
            Directions = new Vector2D[] {                    
                     Vector2D.Right,
                    -Vector2D.Right, 

                     Vector2D.Down, 
                    -Vector2D.Down,

                     Vector2D.DiagDown, 
                    -Vector2D.DiagDown,

                     Vector2D.DiagUp,
                    -Vector2D.DiagUp};
        }
        public Vector2D(int x, int y)
        {
            this.x = x;
            this.y = y;
        }      

        public static Vector2D operator +(Vector2D a, Vector2D b)
        {
            return new Vector2D(a.x + b.x, a.y + b.y);
        }
        public static Vector2D operator -(Vector2D a, Vector2D b)
        {
            return new Vector2D(a.x - b.x, a.y - b.y);
        }
        public static Vector2D operator *(int alpha, Vector2D a)
        {
            return new Vector2D(a.x * alpha, a.y * alpha);
        }
        public static Vector2D operator -(Vector2D a)
        {
            return new Vector2D(-a.x, -a.y);
        }

        public override string ToString()
        {
            return "(" + x + " ," + y + " )";
        }
    }
}
