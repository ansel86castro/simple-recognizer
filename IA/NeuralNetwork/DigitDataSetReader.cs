﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.CommonTypes;

namespace IA.NeuralNetwork
{
    public class DigitDataSetReader:DataSetReader
    {

        protected override float EncodeValue(string value)
        {
            return float.Parse(value) / 16f;
        }

        protected override float[] EncodeClassOutput(float k)
        {
            float[] output = new float[10];
            for (int i = 0; i < 10; i++)
                output[i] = 0.1f;
            output[(int)k] = 0.9f;           
            return output;
        }

        
        protected override float EncodeClass(string value)
        {
            return float.Parse(value);
        }


        protected override string DecodeValue(float value)
        {
            int v = (int)(value * 16);
            return v.ToString();
        }

        protected override string DecodeClass(float clas)
        {
            return clas.ToString();
        }
    }
}
