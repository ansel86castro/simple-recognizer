﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using IA.CommonTypes;

namespace IA.NeuralNetwork
{        

    [Serializable]
    public class Network
    {
        /**/
        Neuron[] neurons;
        Layerlimits[] layers;
        float alpha;
        float epsilon;
        float err=0;
        int loopCount;
        int loopMax;
        bool byLoopCount;
        float randMin, randMax;
        public Network(Neuron[] neurons, Layerlimits[] layers, float alpha, float epsilon, int loopMax, float randMin, float randMax) 
        {
            this.neurons = neurons;
            this.layers = layers;
            this.alpha = alpha;
            this.epsilon = epsilon;
            this.loopMax = loopMax;
            this.randMin = randMin;
            this.randMax = randMax;
            Initialize();
        }
        public Network(Neuron[] neurons, Layerlimits[] layers) : this(neurons, layers, 0.3f, 0.5f, 1000, -0.5f, 0.5f) { }
       
        public Network(int[]layersDescrip, float alpha, float epsilon,int loopMax,float randMin, float randMax)
        {
            this.alpha = alpha;
            this.epsilon = epsilon;
            this.loopMax = loopMax;
            this.randMin = randMin;
            this.randMax = randMax;

            //crear los limites de las capas
            CreateLayers(layersDescrip);

            neurons=new Neuron[layersDescrip.Sum()];

            //crear las conexiones entre las capas de neuronas
            CreateConexions();

            Initialize();
        }
        public Network(int[] layersDescrip) : this(layersDescrip, 0.3f, 0.5f, 1000, -0.5f, 0.5f) { }

        private void CreateConexions()
        {
            //por cada capa 
            for (int k = 1; k < layers.Length; k++)
            {
                //por cada neurona en la capa k
                for (int i = layers[k].start; i <= layers[k].fin; i++)
                {
                    //crear el vector de pesos de la neurona i en la capa k con todas las neuronas de la capa k-1
                    neurons[i] = new Neuron(new NeuronInput[layers[k - 1].Count]);

                    //por cada neurona j en la capa k-1 crear una conexion entre esta y la neurona i de la capa k
                    for (int j = layers[k - 1].start; j <= layers[k - 1].fin; j++)
                    {
                        neurons[i].Inputs[j - layers[k - 1].start].NeuronIndex = j;

                        //en el caso de la capa 0 inicializar las neuronas de entrada
                        if (neurons[j] == null)
                            neurons[j] = new Neuron(null);
                    }
                }
            }
        }
       
        void CreateLayers(int[] layersDescrip)
        {
            layers = new Layerlimits[layersDescrip.Length];
            int previusLayerEnd = -1;

            for (int i = 0; i < layersDescrip.Length; i++)
            {
                layers[i] = new Layerlimits(previusLayerEnd + 1, previusLayerEnd + layersDescrip[i]);
                previusLayerEnd = layers[i].fin;                
            }
        }

        public IEnumerable<IEnumerable<Neuron>> Layers
        {
            get
            {
                for (int i = 0; i < layers.Length; i++)                
                    yield return GetLayer(i);                
            }
        }
       
        public IEnumerable<Neuron> OutputLayer
        {
            get
            {
                return GetLayer(layers.Length - 1);
            }
        }      
        
        public IEnumerable<Neuron> InputLayer
        {
            get
            {
                return GetLayer(0);
            }
        }

        public int Loops { get { return loopMax; } set { loopMax = value; } }
        public float Alpha { get { return alpha; } set { alpha = value; } }
        public float Epsilon { get { return epsilon; } set { epsilon = value; } }
        public float RandMin { get { return randMin; } set { randMin = value; } }
        public float RandMax { get { return randMax; } set { randMax = value; } }

        public IEnumerable<Neuron> GetLayer(int k)
        {
            for (int i = layers[k].start; i <= layers[k].fin; i++)
                yield return neurons[i];
        }

        public float Train(TrainingSet set, MessageHandler notify)
        {
            return Train(set, notify, true);
        }
        
        public float Train(TrainingSet set, MessageHandler notify ,bool byLoopCount)
        {
            this.byLoopCount = byLoopCount;
            float[] output = new float[set[0].Output.Length];
            float totalErr=0;
            this.err = 0;          
            if (set.GroupsCount == 1)
            {
                if (notify != null)
                    notify("Training the network with all examples");

                Initialize();
                Train(set.Group(0), notify,byLoopCount);

                if (notify != null)
                    notify("Validating the network with all examples");

                foreach (var e in set.Group(0))
                {
                    CalculateOutputs(e);
                    err += Error(e.Output);
                    if (notify != null)
                        notify("    Error in Prediction " + err + " of element " + e.Output.InputToString() + " " + OutputValues(layers.Length - 1));
                }
                totalErr += err / set.GroupCount(0);
            }
            else
            {
                for (int i = 0; i < set.GroupsCount; i++)
                {
                    float err = 0;

                    //entrena la red con n-1 grupos de elementos
                    if (notify != null)
                        notify("Training the n -{" + i + "} groups");

                    Initialize();
                    Train(set.KFold_Training(i), notify,byLoopCount);

                    //prueba la red con los grupos restantes y calcula el error como la media de los errores parciales
                    if (notify != null)
                        notify("Validating with group " + i);
                    foreach (var e in set.Group(i))
                    {
                        CalculateOutputs(e);
                        float t=Error(e.Output);
                        err += t;
                        if (notify != null)
                            notify("    Error cuadratic in prediction " + t + " of element " + e.Output.InputToString() + " " + OutputValues(layers.Length - 1));
                    }
                    totalErr += err / set.GroupCount(i);
                }
            }

            return totalErr / set.GroupsCount;
        }
        
        public void Train(IEnumerable<Element> elements, MessageHandler notify)
        {
            Train(elements, notify, true);
        }
        
        public void Train(IEnumerable<Element> elements, MessageHandler notify ,bool byLoopCount)
        {            
            //cantidad de loops
            this.byLoopCount = byLoopCount;
            if(byLoopCount)
                this.loopCount = loopMax;  
            do
            {
                foreach (var e in elements)
                {
                    //calcular la salida de cada neurona el la red
                    CalculateOutputs(e);

                    //calcular el error (lambda) en la capa de salida
                    CalculateOutputErr(e.Output);

                    /* Actualizar la red
                     * backpropagar el error a las capas ocultas
                     * por cada capa desde la capa de salida hasta la capa de entrada
                    */

                    for (int k = layers.Length - 1; k > 0; k--)
                    {
                        //por cada neurona en la capa k propagar a la capa k-1
                        for (int i = layers[k].start; i <= layers[k].fin; i++)
                        {
                            //lambdaj += wij * lambdai  propaga el error a la capa k-1
                            neurons[i].PropagateErr(neurons);

                            //actualiar los pesos de la capa k
                            neurons[i].UpdateWeight(alpha, neurons);
                        }
                    }

                }
            }
            while (!StopCriteria(elements, notify));
        }      

        public void PredictOutput(Element element,MessageHandler notify)
        {
            CalculateOutput(element.Input, element.Output);
            notify("output of network prefiction :" + element.Output.InputToString());
        }              

        public static float ActFunction(float x)
        {
            return 1 / (1 + (float)Math.Exp(-x));
        }

        public static float ActFunctionDer(float x)
        {
            float y = 1 / (1 + (float)Math.Exp(-x));
            return y * (1 - y);
        }              
        
        public float[] CalculateOutput(float[] inputs)
        {
           
            int count = layers[layers.Length - 1].Count;
            float[] output = new float[count];
            CalculateOutput(inputs, output);
            return output;

        }

        public void CalculateOutput(float[] input, float[] output)
        {
            int k = 0;
            for (int i = layers[0].start; i <= layers[0].fin; i++)
            {
                neurons[i].ClearState();
                neurons[i].Output = input[k];
                k++;
            }

            for (int i = 1; i < layers.Length; i++)
            {
                foreach (var n in GetLayer(i))
                {
                    neurons[i].ClearState();
                    n.CalculateOutput(neurons);
                }
            }

            var outputLayer = layers[layers.Length - 1];
            for (int i = outputLayer.start; i <= outputLayer.fin; i++)
            {
                output[i - outputLayer.start] = neurons[i].Output;
            }
        }

        public void Save(string filename)
        {            
            using(FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {                
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, this);
                fs.Flush();
                fs.Close();
            }                            
        }

        public static Network Load(string filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                BinaryFormatter bf = new BinaryFormatter();

                Network net = (Network)bf.Deserialize(fs);              

                return net;
            }

        }

        private void CalculateOutputs(Element element)
        {
            int k = 0;
            for (int i = layers[0].start; i <= layers[0].fin; i++)
            {
                neurons[i].ClearState();
                neurons[i].Output = element.Input[k];
                k++;
            }

            for (int j = 1; j < layers.Length; j++)
            {
                for (int i = layers[j].start; i <= layers[j].fin; i++)
                {
                    neurons[i].ClearState();
                    neurons[i].CalculateOutput(neurons);
                }
            }   
        }

        private void CalculateOutputErr(float[] y)
        {
            var l = layers[layers.Length - 1];
            for (int i = l.start; i <= l.fin; i++)
            {
                float oi = neurons[i].Output;
                neurons[i].Lambda = (y[i - l.start] - oi) * oi * (1 - oi);
            }
        }

        private void Initialize()
        {
            Random r=new Random();
            for (int i = layers[1].start; i < neurons.Length; i++)
                neurons[i].Initialize(r, randMin, randMax);
            
        }

        private bool StopCriteria(IEnumerable<Element> elements, MessageHandler notify)
        {
            err = 0;            
            foreach (var e in elements)
            {
                CalculateOutputs(e);
                err += Error(e.Output);
            }
            err *= 0.5f;

            if (notify != null)
                notify("        Step error " + err);

            //ir disminuyendo el paso 
            //alpha = alpha / 2f;
            if (byLoopCount)
            {
                loopCount--;
                return loopCount < 0;
            }
            return err <= epsilon;
        }
        
        private float Error(float[]y)
        {           
             float err=0;
             var outputLayer = layers[layers.Length - 1];
             for (int i = outputLayer.start; i <= outputLayer.fin; i++)
             {
                 float t = y[i - outputLayer.start] - neurons[i].Output;
                 err += t * t;
             }           
             return err;
        }
        
        public string OutputValues(int layerIndex)
        {
            var outputLayer = layers[layerIndex];

            StringBuilder str = new StringBuilder("Network Output:( " + neurons[outputLayer.start].Output);

            for (int i = outputLayer.start + 1; i <= outputLayer.fin; i++)
            {
                str.Append(", " + neurons[i].Output);
            }
            str.Append(" )");

            return str.ToString();
        }

    }
}
