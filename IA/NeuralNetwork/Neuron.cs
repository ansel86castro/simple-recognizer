﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.NeuralNetwork
{
    [Serializable]
    public class Neuron
    {
        //public int ID;
        public float Output;
        public float Lambda;
        public NeuronInput[] Inputs;
        //public int[] OutPuts;

        public Neuron(NeuronInput[] Inputs)
        {
            //this.ID = id;
            this.Inputs = Inputs;
            this.Lambda = 0;
            this.Output = 0;
           // this.Outputs = outputs;

        }

        public void Initialize(Random r , float min ,float max)
        {
            for (int i = 0; i < Inputs.Length; i++)
            {
                float x = (float)r.NextDouble();
                Inputs[i].Weight = min * (1 - x) + max * x;
            }
        }

        public void Initialize(Random r)
        {
            for (int i = 0; i < Inputs.Length; i++)
            {             
                Inputs[i].Weight = (float)r.NextDouble();
            }
        }

        public void ClearState()
        {
            Lambda = 0;
        }

        public float In(Neuron[] neurons)
        {            
            // In(W,Xj)= SUM( wi,j * xj)
            if (Inputs == null)
                return Output;

            float r=0;
            for (int i = 0; i < Inputs.Length; i++)
            {
                r += Inputs[i].Weight * neurons[Inputs[i].NeuronIndex].Output;
            }
            return r;
        }
        /// <summary>
        /// Actualiza el error Lambda en esta neurona con el error de la neurona (salida) de la cual esta es entrada
        /// </summary>
        /// <param name="w">peso</param>
        /// <param name="error">lambda</param>
        public void UpdateErr(float weight, float lambda)
        {
            Lambda += weight * lambda;
        }
        /// <summary>
        /// Propaga el error a las neuronas de entrada de esta
        /// </summary>
        /// <param name="neurons">nerunas</param>
        internal void PropagateErr(Neuron[] neurons)
        {
            for (int i = 0; i < Inputs.Length; i++)
            {
                //lambdaj = lambdaj + oj * (1 - oj ) * wji * lambdai
                float oi = neurons[Inputs[i].NeuronIndex].Output;
                neurons[Inputs[i].NeuronIndex].Lambda += oi * (1 - oi) * this.Lambda * Inputs[i].Weight;
            }
        }
        /// <summary>
        /// actualiar los pesos
        /// </summary>
        /// <param name="alpha">tasa de aprendizaje</param>
        /// <param name="neurons">neuronas</param>
        public void UpdateWeight(float alpha, Neuron[] neurons)
        {
            for (int i = 0; i < Inputs.Length; i++)
            {             
                //wji = wji + alpha * aj * lambdai
                Inputs[i].Weight += alpha * neurons[Inputs[i].NeuronIndex].Output * Lambda;
            }
        }

        public void CalculateOutput(Neuron[] neurons)
        {
            // output = g(In(x,w))
            Output=Network.ActFunction(In(neurons));
        }
       
    }

    [Serializable]
    public struct NeuronInput
    {
        public int NeuronIndex;
        public float Weight;
        public override string ToString()
        {
            return "Neuron:" + NeuronIndex + ", Value:" + Weight;
        }
    }

    [Serializable]
    public class Layer
    {
        Neuron[] neurons;
        public Layer(Neuron[] neurons)
        {
            this.neurons = neurons;
        }
        public Neuron[] Nuerons { get { return neurons; } }
    }

    [Serializable]
    public struct Layerlimits
    {
        public int start;
        public int fin;

        public Layerlimits(int start, int fin)
        {
            this.start = start;
            this.fin = fin;
        }

        public int Count
        {
            get
            {
                return fin - start + 1;
            }
        }
        public override string ToString()
        {
            return "Start:" + start + ", Fin:" + fin + ", Count:" + Count;
        }
    }

}
