﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.NeuralNetwork;
using IA.CommonTypes;

namespace NNVisualApp
{
    public partial class CreateNetForm : Form
    {
        List<int> layers;

        public CreateNetForm()
        {
            InitializeComponent();
            layers = new List<int>();
        }
        //public int InputLayer { get { return (int)numericUpDown1.Value; } }

        //public int HiddenLayer { get { return (int)numericUpDown2.Value; } }

        //public int OutputLayer { get { return (int)numericUpDown3.Value; } }

        public List<int> Layers { get { return layers; } }

        public float Alpha { get { return (float)numericUpDown4.Value; } }

        public int LoopCount { get { return (int)numericUpDown5.Value; } }

        public float Epsilon { get { return (float)numericUpDown6.Value; } }

        public float RandMin { get { return (float)numericUpDown7.Value; } }

        public float RandMax { get { return (float)numericUpDown8.Value; } }

        private void btCreate_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void CreateNetForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value <= 0)
            {
                MessageBox.Show("layer invalid");
                return;
            }
            layers.Add((int)numericUpDown1.Value);

            lbLayers.Text = "{ " + layers.InputToString() + " }";
        }

    }
}
