﻿namespace NNVisualApp
{
    partial class TrainingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.clase = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.ScreenInput = new System.Windows.Forms.PictureBox();
            this.btClear = new System.Windows.Forms.Button();
            this.btTrain = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.lbExamplesCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.clase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenInput)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Agregar Ejemplo";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // clase
            // 
            this.clase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clase.Location = new System.Drawing.Point(319, 18);
            this.clase.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.clase.Name = "clase";
            this.clase.Size = new System.Drawing.Size(52, 20);
            this.clase.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(259, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Clase";
            // 
            // ScreenInput
            // 
            this.ScreenInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ScreenInput.Location = new System.Drawing.Point(13, 43);
            this.ScreenInput.Name = "ScreenInput";
            this.ScreenInput.Size = new System.Drawing.Size(406, 328);
            this.ScreenInput.TabIndex = 3;
            this.ScreenInput.TabStop = false;
            this.ScreenInput.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ScreenInput_MouseMove);
            this.ScreenInput.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ScreenInput_MouseDown);
            this.ScreenInput.Paint += new System.Windows.Forms.PaintEventHandler(this.ScreenInput_Paint);
            // 
            // btClear
            // 
            this.btClear.Location = new System.Drawing.Point(137, 12);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(75, 23);
            this.btClear.TabIndex = 4;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // btTrain
            // 
            this.btTrain.Location = new System.Drawing.Point(262, 377);
            this.btTrain.Name = "btTrain";
            this.btTrain.Size = new System.Drawing.Size(75, 23);
            this.btTrain.TabIndex = 5;
            this.btTrain.Text = "OK";
            this.btTrain.UseVisualStyleBackColor = true;
            this.btTrain.Click += new System.EventHandler(this.btTrain_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(344, 377);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 6;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // lbExamplesCount
            // 
            this.lbExamplesCount.AutoSize = true;
            this.lbExamplesCount.Location = new System.Drawing.Point(377, 20);
            this.lbExamplesCount.Name = "lbExamplesCount";
            this.lbExamplesCount.Size = new System.Drawing.Size(0, 13);
            this.lbExamplesCount.TabIndex = 7;
            // 
            // TrainingDialog
            // 
            this.AcceptButton = this.btTrain;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btCancel;
            this.ClientSize = new System.Drawing.Size(431, 409);
            this.Controls.Add(this.lbExamplesCount);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btTrain);
            this.Controls.Add(this.btClear);
            this.Controls.Add(this.ScreenInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clase);
            this.Controls.Add(this.button1);
            this.Name = "TrainingDialog";
            this.Text = "TrainingDialog";
            ((System.ComponentModel.ISupportInitialize)(this.clase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown clase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox ScreenInput;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.Button btTrain;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label lbExamplesCount;
    }
}