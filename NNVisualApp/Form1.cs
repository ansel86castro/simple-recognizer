﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.NeuralNetwork;
using IA.CommonTypes;

namespace NNVisualApp
{
    public partial class Form1 : Form
    {
        //array de 0-pixel of ,1-pixel on 
        float[,] input;
        Network net;
        TrainingSet set;
        DigitDataSetReader digDset;
        int resolution = 32;
        Element element;
        int width, height;
        Console console;
        bool bylooopCount;
        public Form1()
        {
            InitializeComponent();
            input = new float[resolution,resolution];
            width = ScreenInput.Width / resolution;
            height = ScreenInput.Height / resolution;

            this.console = new Console();
           
        }

        private void Initialize()
        {
            element = null;
            for (int i = 0; i <  resolution; i++) //i -> filas
            {
                for (int j = 0; j < resolution; j++) //j -> columnas
                {
                    input[i, j] = 0;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ScreenInput_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            CommonActions.DrawLienso(g, input, width, height, resolution);

        }       

        private void loadNetworkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                net = Network.Load(openFileDialog1.FileName);
            }
        }

        private void saveNetworkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog(this);
            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                net.Save(saveFileDialog1.FileName);
            }
        }

        private void loadDataSetToolStripMenuItem_Click(object sender, EventArgs e)
        {                       
            openFileDialog1.ShowDialog(this);

            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                digDset = new DigitDataSetReader();
                digDset.Read(openFileDialog1.FileName);                
            }
        }

        private void groupsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }
        private bool GetDataSet()
        {
            openFileDialog1.ShowDialog(this);

            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                digDset = new DigitDataSetReader();
                digDset.Read(openFileDialog1.FileName);
                return true;
            }
            return false;
        }
        private bool GetTrainingSet()
        {
            if (digDset == null && !GetDataSet())
                 return false;

            GroupsDialog gd = new GroupsDialog();
            gd.ShowDialog(this);
            if (gd.DialogResult == DialogResult.OK)
            {
                set = digDset.GetTraningSet(gd.GroupCount);
                bylooopCount = gd.ByLoopCount;
                return true;
            }
            return false;
        }

        private void checkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (net == null)
            {
                MessageBox.Show("Debe crear una red o cargar una");
                return;
            }
            if (element == null)
            {
                float[,] invert = Miscelanea.Transpose(input);

                float[] convInput = Miscelanea.CompressData(invert,16f);

                element = new Element(convInput, new float[10]);

                CommonActions.DrawLienso(ScreenInput.CreateGraphics(),Miscelanea.Transpose(Miscelanea.ExpandData(convInput, 32, 32)), width, height, resolution);
            }

            net.PredictOutput(element, NetworkHandler);
            lbOutputVector.Text = Miscelanea.GetDigit(element.Output).ToString();
            element = null;
        }             

        private string FormatOutput(float[] p)
        {
            string s = p[0].ToString();

            for (int i = 1; i < p.Length; i++)
            {
                s += " ," + p[i].ToString();
            }
            return s;
        }    

        private void btCheck_Click(object sender, EventArgs e)
        {
            checkToolStripMenuItem_Click(sender, e);
        }

        private void ScreenInput_MouseDown(object sender, MouseEventArgs e)
        {

            CommonActions.DrawPixel(input, width, height, e);
            ScreenInput.Refresh();
        }

        private void ScreenInput_MouseMove(object sender, MouseEventArgs e)
        {
            CommonActions.DrawPixel(input, width, height, e);
            ScreenInput.Refresh();
        }

        private void clearScreen_Click(object sender, EventArgs e)
        {
            CommonActions.Initialize(input);
            ScreenInput.Refresh();
        }

        private void dataSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (net == null)
            {
                if (!CreateNetWork()) return;
            }
            if (!GetTrainingSet()) return;

            float error=net.Train(set, NetworkHandler);

            float presition = (1 - error) * 100;
            MessageBox.Show("Presition Achieved of " + presition + "%");            
        }
        void NetworkHandler(string value)
        {
            console.WriteLine(value);
        }
        private void dataSetStaticsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void visualizeWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExamplePicker ePicker = new ExamplePicker(digDset);

            ePicker.ShowDialog(this);

            if (ePicker.DialogResult == DialogResult.OK)
            {
                if (digDset == null && !GetDataSet()) return;

                element = digDset.GetElement(ePicker.Class, ePicker.Example);
                Miscelanea.ExpandData(element.Input, input);

                input = Miscelanea.Transpose(input);

                ScreenInput.Refresh();
            }

        }

        private void consoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            if (console.Disposing)
                console = new Console();
            try
            {
                if (menuItem != null)
                {
                    if (menuItem.Checked)
                    {
                        menuItem.Checked = false;
                        this.console.Hide();
                    }
                    else
                    {
                        menuItem.Checked = true;
                        this.console.Show();
                    }

                }
            }
            catch (Exception)
            {
                console = new Console();
                menuItem.Checked = true;
                console.Show();
            }
        }

        private void manualyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrainingDialog trainingWindow = new TrainingDialog(digDset,resolution);
            trainingWindow.ShowDialog(this);
            if (trainingWindow.DialogResult == DialogResult.OK)
            {
                digDset = trainingWindow.DataReader;

                dataSetToolStripMenuItem_Click(sender, e);                
            }
        }

        private void trainingWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrainingDialog trainingWindow = new TrainingDialog(digDset, resolution);
            trainingWindow.ShowDialog(this);
            if (trainingWindow.DialogResult == DialogResult.OK)
            {
                digDset = trainingWindow.DataReader;             
            }
        }

        private void saveDataSetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (digDset != null)
            {
                saveFileDialog1.ShowDialog(this);
                if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
                {
                    digDset.Save(saveFileDialog1.FileName);
                }
            }
        }

        private void createNetworkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateNetWork();   
        }
        bool CreateNetWork()
        {
            CreateNetForm form = new CreateNetForm();
            form.ShowDialog(this);
            if (form.DialogResult == DialogResult.OK)
            {
                net = new Network(form.Layers.ToArray(), form.Alpha, form.Epsilon, form.LoopCount, form.RandMin, form.RandMax);
                return true;
            }
            return false;
        }
    }
}
