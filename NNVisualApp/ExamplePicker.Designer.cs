﻿namespace NNVisualApp
{
    partial class ExamplePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nClass = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nExample = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.lbExamplesCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nExample)).BeginInit();
            this.SuspendLayout();
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(27, 122);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(62, 23);
            this.OK.TabIndex = 0;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(95, 122);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(69, 23);
            this.Cancel.TabIndex = 1;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Class";
            // 
            // nClass
            // 
            this.nClass.Location = new System.Drawing.Point(95, 16);
            this.nClass.Name = "nClass";
            this.nClass.Size = new System.Drawing.Size(54, 20);
            this.nClass.TabIndex = 3;
            this.nClass.ValueChanged += new System.EventHandler(this.nClass_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Example";
            // 
            // nExample
            // 
            this.nExample.Location = new System.Drawing.Point(95, 69);
            this.nExample.Name = "nExample";
            this.nExample.Size = new System.Drawing.Size(54, 20);
            this.nExample.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Examples Count";
            // 
            // lbExamplesCount
            // 
            this.lbExamplesCount.AutoSize = true;
            this.lbExamplesCount.Location = new System.Drawing.Point(113, 40);
            this.lbExamplesCount.Name = "lbExamplesCount";
            this.lbExamplesCount.Size = new System.Drawing.Size(0, 13);
            this.lbExamplesCount.TabIndex = 7;
            // 
            // ExamplePicker
            // 
            this.AcceptButton = this.OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(202, 148);
            this.Controls.Add(this.lbExamplesCount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nExample);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nClass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Name = "ExamplePicker";
            this.Text = "Visualize a dataset example";
            ((System.ComponentModel.ISupportInitialize)(this.nClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nExample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nClass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nExample;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbExamplesCount;
    }
}