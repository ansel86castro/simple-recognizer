﻿namespace NNVisualApp
{
    partial class Console
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConsoleScreen = new System.Windows.Forms.TextBox();
            this.btClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ConsoleScreen
            // 
            this.ConsoleScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ConsoleScreen.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ConsoleScreen.ForeColor = System.Drawing.Color.MintCream;
            this.ConsoleScreen.Location = new System.Drawing.Point(13, 38);
            this.ConsoleScreen.Multiline = true;
            this.ConsoleScreen.Name = "ConsoleScreen";
            this.ConsoleScreen.ReadOnly = true;
            this.ConsoleScreen.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ConsoleScreen.Size = new System.Drawing.Size(356, 293);
            this.ConsoleScreen.TabIndex = 0;
            this.ConsoleScreen.Text = "Digit Neural Network Report Console 1.0";
            this.ConsoleScreen.WordWrap = false;
            // 
            // btClear
            // 
            this.btClear.Location = new System.Drawing.Point(13, 13);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(61, 23);
            this.btClear.TabIndex = 1;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            this.btClear.Click += new System.EventHandler(this.btClear_Click);
            // 
            // Console
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 343);
            this.Controls.Add(this.btClear);
            this.Controls.Add(this.ConsoleScreen);
            this.Name = "Console";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Console";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ConsoleScreen;
        private System.Windows.Forms.Button btClear;
    }
}