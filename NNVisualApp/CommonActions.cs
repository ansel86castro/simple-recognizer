﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using IA.NeuralNetwork;
using IA.CommonTypes;

namespace NNVisualApp
{
    class CommonActions
    {
         public static void DrawLienso(Graphics g, float[,] input, int width, int height, int resolution)
        {
            g.Clear(Color.White);
            for (int i = 0; i < resolution; i++) //i -> filas
            {
                for (int j = 0; j < resolution; j++) //j -> columnas
                {
                    SolidBrush b = new SolidBrush(Miscelanea.GetColor(input[i, j]));
                    g.FillRectangle(b, i * width, j * height, width, height);
                }
            }
        }
         public static void DrawPixel(float[,] input, int width, int height, MouseEventArgs e)
        {

            int i = e.X / width;
            int j = e.Y / height;
            if (e.Button == MouseButtons.Left)
            {
                DrawRegion(1, input, new Vector2D(i, j), 1, Vector2D.Directions);
                
            }
            else if (e.Button == MouseButtons.Right)
            {
                DrawRegion(1, input, new Vector2D(i, j), 0, Vector2D.Directions);
            }            
        }
         public static void DrawRegion(int paso, float[,] input, Vector2D pos ,float color ,Vector2D[]directions)
         {
             int finalX = input.GetLength(0), finalY = input.GetLength(1);
             if (isInto(pos, finalX, finalY))
             {
                 input[pos.x, pos.y] = color;
                 for (int i = 0; i < directions.Length; i++)
                 {
                     Vector2D nextPos = pos + paso * directions[i];
                     if (isInto(nextPos, finalX, finalY))
                     {
                         input[nextPos.x, nextPos.y] = color;
                     }
                 }
             }
         }
        private static bool isInto(Vector2D pos, int finalX ,int finalY)
        {
            return (pos.x < finalX && pos.x >= 0) && (pos.y >= 0 && pos.y < finalY);
        }
         public static void Initialize(float[,] input)
        {         
            for (int i = 0; i < input.GetLength(0); i++) //i -> filas
            {
                for (int j = 0; j < input.GetLength(1); j++) //j -> columnas
                {
                    input[i, j] = 0;
                }
            }
        }
    }
}
