﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.NeuralNetwork;
using IA.CommonTypes;

namespace NNVisualApp
{
    public partial class ExamplePicker : Form
    {
        DataSetReader dSet;
        public ExamplePicker(DataSetReader dSet)
        {
            InitializeComponent();

            this.dSet = dSet;
        }
        public float Class
        {
            get
            {
                return (float)nClass.Value;
            }
        }
        public int Example
        {
            get
            {
                return (int)nExample.Value;
            }
        }
        private void OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void nClass_ValueChanged(object sender, EventArgs e)
        {
            lbExamplesCount.Text=dSet.GetData((float)nExample.Value).Count.ToString();
        }

    }
}
