﻿namespace NNVisualApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadNetworkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveNetworkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDataSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveDataSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.netwokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainWithDataSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSetStaticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainingWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualizeWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btCheck = new System.Windows.Forms.ToolStripButton();
            this.clearScreen = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lbOutputVector = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.lbOutputValue = new System.Windows.Forms.Label();
            this.ScreenInput = new System.Windows.Forms.PictureBox();
            this.createNetworkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenInput)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.dataToolStripMenuItem,
            this.netwokToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(452, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadNetworkToolStripMenuItem,
            this.saveNetworkToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadNetworkToolStripMenuItem
            // 
            this.loadNetworkToolStripMenuItem.Name = "loadNetworkToolStripMenuItem";
            this.loadNetworkToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadNetworkToolStripMenuItem.Text = "Load Network";
            this.loadNetworkToolStripMenuItem.Click += new System.EventHandler(this.loadNetworkToolStripMenuItem_Click);
            // 
            // saveNetworkToolStripMenuItem
            // 
            this.saveNetworkToolStripMenuItem.Name = "saveNetworkToolStripMenuItem";
            this.saveNetworkToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveNetworkToolStripMenuItem.Text = "Save Network";
            this.saveNetworkToolStripMenuItem.Click += new System.EventHandler(this.saveNetworkToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDataSetToolStripMenuItem,
            this.saveDataSetToolStripMenuItem,
            this.groupsToolStripMenuItem});
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // loadDataSetToolStripMenuItem
            // 
            this.loadDataSetToolStripMenuItem.Name = "loadDataSetToolStripMenuItem";
            this.loadDataSetToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.loadDataSetToolStripMenuItem.Text = "Load DataSet";
            this.loadDataSetToolStripMenuItem.Click += new System.EventHandler(this.loadDataSetToolStripMenuItem_Click);
            // 
            // saveDataSetToolStripMenuItem
            // 
            this.saveDataSetToolStripMenuItem.Name = "saveDataSetToolStripMenuItem";
            this.saveDataSetToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveDataSetToolStripMenuItem.Text = "Save DataSet";
            this.saveDataSetToolStripMenuItem.Click += new System.EventHandler(this.saveDataSetToolStripMenuItem_Click);
            // 
            // groupsToolStripMenuItem
            // 
            this.groupsToolStripMenuItem.Name = "groupsToolStripMenuItem";
            this.groupsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.groupsToolStripMenuItem.Text = "Groups";
            this.groupsToolStripMenuItem.Click += new System.EventHandler(this.groupsToolStripMenuItem_Click);
            // 
            // netwokToolStripMenuItem
            // 
            this.netwokToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainWithDataSetToolStripMenuItem,
            this.checkToolStripMenuItem,
            this.createNetworkToolStripMenuItem});
            this.netwokToolStripMenuItem.Name = "netwokToolStripMenuItem";
            this.netwokToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.netwokToolStripMenuItem.Text = "Netwok";
            // 
            // trainWithDataSetToolStripMenuItem
            // 
            this.trainWithDataSetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataSetToolStripMenuItem,
            this.manualyToolStripMenuItem});
            this.trainWithDataSetToolStripMenuItem.Name = "trainWithDataSetToolStripMenuItem";
            this.trainWithDataSetToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.trainWithDataSetToolStripMenuItem.Text = "Train";
            // 
            // dataSetToolStripMenuItem
            // 
            this.dataSetToolStripMenuItem.Name = "dataSetToolStripMenuItem";
            this.dataSetToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.dataSetToolStripMenuItem.Text = "From DataSet";
            this.dataSetToolStripMenuItem.Click += new System.EventHandler(this.dataSetToolStripMenuItem_Click);
            // 
            // manualyToolStripMenuItem
            // 
            this.manualyToolStripMenuItem.Name = "manualyToolStripMenuItem";
            this.manualyToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.manualyToolStripMenuItem.Text = "From User Input";
            this.manualyToolStripMenuItem.Click += new System.EventHandler(this.manualyToolStripMenuItem_Click);
            // 
            // checkToolStripMenuItem
            // 
            this.checkToolStripMenuItem.Name = "checkToolStripMenuItem";
            this.checkToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.checkToolStripMenuItem.Text = "Check";
            this.checkToolStripMenuItem.Click += new System.EventHandler(this.checkToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consoleToolStripMenuItem,
            this.dataSetStaticsToolStripMenuItem,
            this.trainingWindowToolStripMenuItem,
            this.visualizeWindowToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // consoleToolStripMenuItem
            // 
            this.consoleToolStripMenuItem.Name = "consoleToolStripMenuItem";
            this.consoleToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.consoleToolStripMenuItem.Text = "Console";
            this.consoleToolStripMenuItem.Click += new System.EventHandler(this.consoleToolStripMenuItem_Click);
            // 
            // dataSetStaticsToolStripMenuItem
            // 
            this.dataSetStaticsToolStripMenuItem.Name = "dataSetStaticsToolStripMenuItem";
            this.dataSetStaticsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.dataSetStaticsToolStripMenuItem.Text = "DataSet Statics";
            this.dataSetStaticsToolStripMenuItem.Click += new System.EventHandler(this.dataSetStaticsToolStripMenuItem_Click);
            // 
            // trainingWindowToolStripMenuItem
            // 
            this.trainingWindowToolStripMenuItem.Name = "trainingWindowToolStripMenuItem";
            this.trainingWindowToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.trainingWindowToolStripMenuItem.Text = "Training Window";
            this.trainingWindowToolStripMenuItem.Click += new System.EventHandler(this.trainingWindowToolStripMenuItem_Click);
            // 
            // visualizeWindowToolStripMenuItem
            // 
            this.visualizeWindowToolStripMenuItem.Name = "visualizeWindowToolStripMenuItem";
            this.visualizeWindowToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.visualizeWindowToolStripMenuItem.Text = "Visualize Window";
            this.visualizeWindowToolStripMenuItem.Click += new System.EventHandler(this.visualizeWindowToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btCheck,
            this.clearScreen});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(452, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btCheck
            // 
            this.btCheck.BackColor = System.Drawing.Color.SkyBlue;
            this.btCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btCheck.Image = ((System.Drawing.Image)(resources.GetObject("btCheck.Image")));
            this.btCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCheck.Name = "btCheck";
            this.btCheck.Size = new System.Drawing.Size(40, 22);
            this.btCheck.Text = "Check";
            this.btCheck.Click += new System.EventHandler(this.btCheck_Click);
            // 
            // clearScreen
            // 
            this.clearScreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.clearScreen.Image = ((System.Drawing.Image)(resources.GetObject("clearScreen.Image")));
            this.clearScreen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearScreen.Name = "clearScreen";
            this.clearScreen.Size = new System.Drawing.Size(36, 22);
            this.clearScreen.Text = "Clear";
            this.clearScreen.Click += new System.EventHandler(this.clearScreen_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Prediction";
            // 
            // lbOutputVector
            // 
            this.lbOutputVector.AutoSize = true;
            this.lbOutputVector.Location = new System.Drawing.Point(77, 53);
            this.lbOutputVector.Name = "lbOutputVector";
            this.lbOutputVector.Size = new System.Drawing.Size(92, 13);
            this.lbOutputVector.TabIndex = 4;
            this.lbOutputVector.Text = "do click on check";
            // 
            // lbOutputValue
            // 
            this.lbOutputValue.AutoSize = true;
            this.lbOutputValue.Location = new System.Drawing.Point(231, 53);
            this.lbOutputValue.Name = "lbOutputValue";
            this.lbOutputValue.Size = new System.Drawing.Size(35, 13);
            this.lbOutputValue.TabIndex = 5;
            this.lbOutputValue.Text = "label2";
            // 
            // ScreenInput
            // 
            this.ScreenInput.Location = new System.Drawing.Point(34, 69);
            this.ScreenInput.Name = "ScreenInput";
            this.ScreenInput.Size = new System.Drawing.Size(383, 305);
            this.ScreenInput.TabIndex = 6;
            this.ScreenInput.TabStop = false;
            this.ScreenInput.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ScreenInput_MouseMove);
            this.ScreenInput.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ScreenInput_MouseDown);
            this.ScreenInput.Paint += new System.Windows.Forms.PaintEventHandler(this.ScreenInput_Paint);
            // 
            // createNetworkToolStripMenuItem
            // 
            this.createNetworkToolStripMenuItem.Name = "createNetworkToolStripMenuItem";
            this.createNetworkToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.createNetworkToolStripMenuItem.Text = "Create Network";
            this.createNetworkToolStripMenuItem.Click += new System.EventHandler(this.createNetworkToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 386);
            this.Controls.Add(this.ScreenInput);
            this.Controls.Add(this.lbOutputValue);
            this.Controls.Add(this.lbOutputVector);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScreenInput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadNetworkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveNetworkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDataSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveDataSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem netwokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainWithDataSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btCheck;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbOutputVector;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consoleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSetStaticsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainingWindowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualizeWindowToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem groupsToolStripMenuItem;
        private System.Windows.Forms.Label lbOutputValue;
        private System.Windows.Forms.ToolStripButton clearScreen;
        private System.Windows.Forms.PictureBox ScreenInput;
        private System.Windows.Forms.ToolStripMenuItem createNetworkToolStripMenuItem;
    }
}

