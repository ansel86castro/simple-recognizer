﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace NNVisualApp
{
    public partial class Console : Form
    {
        StringWriter outStream;
        public Console()
        {
            InitializeComponent();
            outStream = new StringWriter();
            outStream.WriteLine("Digit Neural Network Report Console 1.0");
            ConsoleScreen.Text = outStream.ToString();
        }
        public void WriteLine(string value)
        {
            outStream.WriteLine(value);
            ConsoleScreen.Text = outStream.ToString();
        }

        private void btClear_Click(object sender, EventArgs e)
        {
            var buffer = outStream.GetStringBuilder();
            buffer.Remove(0, buffer.Length);           
            ConsoleScreen.Text = "Digit Neural Network Report Console 1.0";
        }
    }
}
